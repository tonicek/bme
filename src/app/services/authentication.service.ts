import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginPayload, LoginResponse, LoginError } from '../models/login.model';
import { Observable, of } from 'rxjs';

const LOGIN_API =
  'https://monitoralpha.keeeb.com/auth/api/v1.0/auth/jwt/create/?service=https://monitoralpha.keeeb.com';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private http: HttpClient) {}

  login(payload: LoginPayload) {
    return of(
      payload.email === 'testuser1@keeeb.com' && payload.password === 'password'
        ? {
            token: 'eyJSsdasd123jsdas',
            service: 'http://monitoralpha.keeeb.com/',
            refresh_token: '69ba6546e678bdcc64e635adc6eca2008eb9c5ba',
          }
        : {
            error: 'Unable to log in with provided credentials.',
          }
    );
  }
}
