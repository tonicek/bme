import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BookmarkRequest } from '../models/mbs.model';

const SEARCH_API = 'assets/data/result.json';

@Injectable({
  providedIn: 'root',
})
export class TopicService {
  constructor(private http: HttpClient) {}

  search() {
    return this.http.get(SEARCH_API);
  }

  getBookmark(req: BookmarkRequest) {
    return this.http.get(`assets/data/${req.page}_${req.bookmark}.json`);
  }
}
