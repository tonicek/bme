import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlatformGuard } from './modules/platform/guards/platform.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: './modules/platform/platform.module#PlatformModule',
    canActivate: [PlatformGuard]
  },
  {
    path: 'authentication',
    loadChildren: './modules/authentication/authentication.module#AuthenticationModule'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
