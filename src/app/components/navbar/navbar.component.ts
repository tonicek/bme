import { Component, OnInit } from '@angular/core';
import { TopicState } from 'src/app/states/topic.state';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @Select(TopicState.breadcrumb)
  breadcrumb$: Observable<String>;

  constructor() {}

  signOut() {
    localStorage.clear();
    location.reload();
  }

  ngOnInit() {}
}
