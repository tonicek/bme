import { Component, OnInit } from '@angular/core';
import { TopicState } from 'src/app/states/topic.state';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { Topic } from 'src/app/models/topic.model';
import { SelectTopic } from 'src/app/actions/topic.actions';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  @Select(TopicState.loading)
  loading$: Observable<boolean>;

  @Select(TopicState.topics)
  topics$: Observable<Topic[]>;

  isMobile = false;

  constructor(private store: Store, private breakpointObserver: BreakpointObserver) {
    this.breakpointObserver.observe([Breakpoints.XSmall]).subscribe(result => {
      this.isMobile = result.matches;
    });
  }

  selectTopic(topic: Topic) {
    this.store.dispatch(new SelectTopic(topic));
  }

  ngOnInit() {}
}
