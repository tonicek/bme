import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkeletonPlaceholderComponent } from './skeleton-placeholder.component';

describe('SkeletonPlaceholderComponent', () => {
  let component: SkeletonPlaceholderComponent;
  let fixture: ComponentFixture<SkeletonPlaceholderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkeletonPlaceholderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkeletonPlaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
