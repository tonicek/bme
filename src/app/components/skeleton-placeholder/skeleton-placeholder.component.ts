import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton-placeholder',
  templateUrl: './skeleton-placeholder.component.html',
  styleUrls: ['./skeleton-placeholder.component.scss']
})
export class SkeletonPlaceholderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
