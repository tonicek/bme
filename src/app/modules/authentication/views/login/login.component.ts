import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { Login } from 'src/app/actions/authentication.actions';
import { AuthenticationState } from 'src/app/states/authentication.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  @Select(AuthenticationState.loading)
  $loading: Observable<boolean>;

  constructor(private fb: FormBuilder, private store: Store) {
    this._initializeForm();
  }

  ngOnInit() {}

  login() {
    this.store.dispatch(new Login(this.loginForm.value));
  }

  private _initializeForm() {
    this.loginForm = this.fb.group({
      email: [null, Validators.compose([Validators.email, Validators.required])],
      password: [null, Validators.required],
    });
  }
}
