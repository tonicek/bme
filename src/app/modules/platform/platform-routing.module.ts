import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { PageComponent } from './views/page/page.component';
import { MbsComponent } from './views/mbs/mbs.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: PageComponent,
        data: {
          animation: 'fader',
        },
      },
      {
        path: 'page/:page/mbs/:mbs',
        component: MbsComponent,
        data: {
          animation: 'fader',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlatformRoutingModule {}
