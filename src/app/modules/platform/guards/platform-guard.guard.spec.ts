import { TestBed, async, inject } from '@angular/core/testing';

import { PlatformGuard } from './platform.guard';

describe('PlatformGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlatformGuard]
    });
  });

  it('should ...', inject([PlatformGuard], (guard: PlatformGuard) => {
    expect(guard).toBeTruthy();
  }));
});
