import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlatformRoutingModule } from './platform-routing.module';
import { HomeComponent } from './views/home/home.component';
import { SharedModule } from '../shared/shared.module';
import { PageComponent } from './views/page/page.component';
import { MbsComponent } from './views/mbs/mbs.component';
import { GroupComponent } from './views/group/group.component';

@NgModule({
  declarations: [HomeComponent, PageComponent, MbsComponent, GroupComponent],
  imports: [CommonModule, SharedModule, PlatformRoutingModule],
})
export class PlatformModule {}
