import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { GetBookmark } from 'src/app/actions/topic.actions';
import { TopicState } from 'src/app/states/topic.state';
import { Observable } from 'rxjs';
import { MicroBookmarkImage, MicroBookmarkText } from 'src/app/models/mbs.model';
import { fader } from '../../route-animations';

@Component({
  selector: 'app-mbs',
  templateUrl: './mbs.component.html',
  styleUrls: ['./mbs.component.scss'],
  animations: [fader],
})
export class MbsComponent implements OnInit {
  @Select(TopicState.currentBookmark)
  bookmark$: Observable<MicroBookmarkImage | MicroBookmarkText>;

  constructor(private route: ActivatedRoute, private router: Router, private store: Store) {
    const { mbs, page } = route.snapshot.params;
    this.store.dispatch(
      new GetBookmark({
        bookmark: mbs,
        page: page,
      })
    );
  }

  ngOnInit() {}
}
