import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MbsComponent } from './mbs.component';

describe('MbsComponent', () => {
  let component: MbsComponent;
  let fixture: ComponentFixture<MbsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MbsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
