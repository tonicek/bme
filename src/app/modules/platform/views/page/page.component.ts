import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { TopicState } from 'src/app/states/topic.state';
import { Observable } from 'rxjs';
import { Topic } from 'src/app/models/topic.model';
import { fader } from '../../route-animations';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  animations: [fader],
})
export class PageComponent implements OnInit {
  @Select(TopicState.currentTopic)
  topic$: Observable<Topic>;

  constructor() {}

  ngOnInit() {}
}
