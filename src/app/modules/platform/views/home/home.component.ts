import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { SearchQuery } from 'src/app/actions/topic.actions';
import { RouterOutlet } from '@angular/router';
import { fader } from '../../route-animations';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [fader],
})
export class HomeComponent implements OnInit {
  isMobile = false;

  constructor(private store: Store, private breakpointObserver: BreakpointObserver) {
    this.store.dispatch(new SearchQuery());

    this.breakpointObserver.observe([Breakpoints.XSmall]).subscribe(result => {
      this.isMobile = result.matches;
    });
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  ngOnInit() {}
}
