import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from 'src/app/components/navbar/navbar.component';
import { SidebarComponent } from 'src/app/components/sidebar/sidebar.component';
import { ContentComponent } from 'src/app/components/content/content.component';
import { SkeletonPlaceholderComponent } from 'src/app/components/skeleton-placeholder/skeleton-placeholder.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [NavbarComponent, SidebarComponent, ContentComponent, SkeletonPlaceholderComponent],
  imports: [CommonModule, TranslateModule, FormsModule, ReactiveFormsModule],
  exports: [
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    NavbarComponent,
    SidebarComponent,
    ContentComponent,
    SkeletonPlaceholderComponent,
  ],
})
export class SharedModule {}
