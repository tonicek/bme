import { LoginPayload } from '../models/login.model';

export class Login {
  static readonly type = '[AUTHENTICATION] Login';
  constructor(public readonly payload: LoginPayload) {}
}
