import { Page } from '../models/page.model';
import { Topic } from '../models/topic.model';
import { MicroBookmarkImage, MicroBookmarkText, BookmarkRequest } from '../models/mbs.model';

export class SearchQuery {
  static readonly type = '[Search] SearchQuery]';
}
export class SelectTopic {
  static readonly type = '[Search] SelectTopic]';
  constructor(public readonly topic: Topic) {}
}
export class SelectPage {
  static readonly type = '[Search] SelectPage]';
  constructor(public readonly page: Page) {}
}
export class GetBookmark {
  static readonly type = '[Search] SelectedBookmark]';
  constructor(public readonly payload: BookmarkRequest) {}
}
