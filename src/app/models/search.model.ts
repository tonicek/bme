import { Topic } from './topic.model';

export interface SearchResponse {
  metadata: any;
  data: Array<Topic>;
  paging: any;
}
