import { Page } from './page.model';
import { MicroBookmark } from './mbs.model';

export interface Topic {
  date_last_updated: string;
  date_created: string;
  url: string;
  date_retrieved: string;
  type: string;
  id: string;
  title: string;
  self_url: string;
  page: Page;
  mbs: MicroBookmark[];
}
