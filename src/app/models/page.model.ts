export interface Page {
  date_created: string;
  date_last_updated: string;
  microbookmarks: number;
  name: string;
  self_url: string;
  id: string;
}
