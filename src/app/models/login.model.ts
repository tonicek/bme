export interface LoginPayload {
  email: string;
  password: string;
}

export interface LoginResponse {
  token: string;
  service: string;
  refresh_token: string;
}

export interface LoginError {
  message: string;
}
