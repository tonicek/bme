export interface MicroBookmark {
  date_changed: string;
  date_retrieved: string;
  group_id: string;
  group_supertitle: string;
  id: string;
  self_url: string;
  title: string;
  type: 'text' | 'image';
}

export interface MicroBookmarkText extends MicroBookmark {
  html: string;
  text: string;
}

export interface MicroBookmarkImage extends MicroBookmark {
  images: object;
}

export interface BookmarkRequest {
  page: string;
  bookmark: string;
}
