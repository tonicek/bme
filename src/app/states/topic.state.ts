import { State, Selector, Action, StateContext, Actions, ofActionDispatched } from '@ngxs/store';
import { Topic } from '../models/topic.model';
import { TopicService } from '../services/topic.service';
import { SearchQuery, SelectTopic, SelectPage, GetBookmark } from '../actions/topic.actions';
import { SearchResponse } from '../models/search.model';
import { MicroBookmark, MicroBookmarkText, MicroBookmarkImage } from '../models/mbs.model';
import { NgZone } from '@angular/core';
import { Router } from '@angular/router';

export interface TopicStateModel {
  loading: boolean;
  topics: Array<Topic>;
  currentTopic: Topic;
  bookmarks: Array<MicroBookmark>;
  currentBookmark: MicroBookmarkText | MicroBookmarkImage;
  breadcrumb: string;
}

@State<TopicStateModel>({
  name: 'search',
  defaults: {
    loading: true,
    topics: [],
    currentTopic: null,
    bookmarks: null,
    currentBookmark: null,
    breadcrumb: '',
  },
})
export class TopicState {
  constructor(private topicService: TopicService, private router: Router, private zone: NgZone) {}

  @Selector()
  static topics({ topics }: TopicStateModel) {
    return topics;
  }

  @Selector()
  static breadcrumb({ breadcrumb }: TopicStateModel) {
    return breadcrumb;
  }

  @Selector()
  static loading({ loading }: TopicStateModel) {
    return loading;
  }

  @Selector()
  static currentTopic({ currentTopic }: TopicStateModel) {
    return currentTopic;
  }

  @Selector()
  static bookmarks({ bookmarks }: TopicStateModel) {
    return bookmarks;
  }

  @Selector()
  static currentBookmark({ currentBookmark }: TopicStateModel) {
    return currentBookmark;
  }

  @Action(SearchQuery)
  searchQuery({ patchState, dispatch }: StateContext<TopicStateModel>) {
    return this.topicService.search().subscribe((res: SearchResponse) => {
      patchState({
        topics: res.data,
        loading: false,
      });
      if (res.data) {
        dispatch(new SelectTopic(res.data[0]));
      }
    });
  }

  @Action(SelectTopic)
  selectTopic({ patchState }: StateContext<TopicStateModel>, { topic }: SelectTopic) {
    patchState({
      currentTopic: topic,
      breadcrumb: topic.title,
    });

    this.zone.run(() => this.router.navigateByUrl('/'));
  }

  @Action(GetBookmark)
  selectedBookmark(
    { patchState, getState }: StateContext<TopicStateModel>,
    { payload }: GetBookmark
  ) {
    patchState({
      currentBookmark: null,
    });
    return this.topicService
      .getBookmark(payload)
      .subscribe((bookmark: MicroBookmarkImage | MicroBookmarkText) => {
        const { currentTopic } = getState();
        patchState({
          currentBookmark: bookmark,
          breadcrumb: [currentTopic.title, currentTopic.page.name, bookmark.title].join(' > '),
        });
      });
  }
}
