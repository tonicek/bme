import { State, Action, StateContext, Selector } from '@ngxs/store';
import { AuthenticationService } from '../services/authentication.service';
import { Login } from '../actions/authentication.actions';
import { tap } from 'rxjs/operators';
import { NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

export interface AuthenticationStateModel {
  loading: boolean;
}

@State<AuthenticationStateModel>({
  name: 'authentication',
  defaults: {
    loading: false,
  },
})
export class AuthenticationState {
  constructor(
    private authenticationService: AuthenticationService,
    private zone: NgZone,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  @Selector()
  static loading({ loading }: AuthenticationStateModel) {
    return loading;
  }

  @Action(Login)
  login(
    { getState, dispatch, patchState }: StateContext<AuthenticationStateModel>,
    { payload }: Login
  ) {
    return this.authenticationService.login(payload).subscribe(result => {
      patchState({
        loading: false,
      });

      if (result.error) {
        return this.zone.run(() => this.snackBar.open(result.error, '', { duration: 3000 }));
      }

      localStorage['refresh_token'] = result.refresh_token;
      localStorage['service'] = result.service;
      localStorage['token'] = result.token;

      patchState({
        loading: true,
      });

      this.zone.run(() => this.router.navigateByUrl('/'));
    });
  }
}
